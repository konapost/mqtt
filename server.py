import paho.mqtt.client as mqtt
import uuid
import time
from datetime import datetime
import random
import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017")
mydb = myclient["mqtt"]
mycol = mydb["from_client"]


def on_message(client, userdata, message):
    x = str(message.payload.decode("utf-8"))
    print(x)
    mydict = { "message": x }
    mycol.insert_one(mydict)


def sub_func():    
    client.loop_start()
    client.subscribe("Clientside")
    client.on_message = on_message
    time.sleep(1)
    client.loop_stop()

mqttBroker = "mqtt.eclipseprojects.io"
client = mqtt.Client("server")
client.connect(mqttBroker)



msg_list = ["erc","drc","res"]
seqNum = 1
while True:
    msg_type = random.choice(msg_list)
    timestamp = datetime.now()
    msg = str(seqNum) + " " + str(timestamp) + " " + str(msg_type)
    client.publish("Serverside", msg)
    sub_func()
    time.sleep(1)
    seqNum = seqNum + 1 
    