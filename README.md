**This is a python client server approach based on mqtt.**

Firstly, need to install the appropriate python libraries. 
To do so we execute the command:
`pip install -r requirements.txt`

Note that client and server messages are also logged in a **mondoDB** so our system needs to have it installed.

In order to run the programm we execute the files client.py and server.py

Please download the demo.mkv file to see a demonstration of the code execution.

